import { Card, CircularProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-client-preset';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import * as firebase from 'firebase/app';
import React from 'react';
import { connect } from 'react-redux';
import {
    AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK, AUTH_GET_PERMISSIONS,
    userLogin, Notification
} from 'react-admin';
import "firebase/auth";
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';

const firebaseApp = firebase.initializeApp({
    // cSpell:disable
    apiKey: "AIzaSyAElZEHF7S3MH0XiJS_H6T44bt7Cnq2_AI",
    authDomain: "hazel-cedar-540.firebaseapp.com",
    databaseURL: "https://hazel-cedar-540.firebaseio.com",
    projectId: "hazel-cedar-540",
    storageBucket: "hazel-cedar-540.appspot.com",
    messagingSenderId: "446067840392"
    // cSpell:enable
});

export const client = new ApolloClient({
    // uri: 'http://localhost:4000/',
    link: setContext((_, { headers }) => {
        const token = sessionStorage.getItem('userToken');
        return {
            headers: {
                ...headers,
                authorization: token ? `Bearer ${token}` : "",
            }
        };
    }).concat(createHttpLink({
        uri: 'http://localhost:4000/',
        //uri: 'https://eu1.prisma.sh/flavian/ra-data-prisma/dev'
    })),
    cache: new InMemoryCache().restore({})
});

const signOut = async () => {
    sessionStorage.removeItem('userToken');
    sessionStorage.removeItem('userName');
    sessionStorage.removeItem('userRole');
    client.resetStore();
    return firebaseApp.auth().signOut();    
};

export function authProvider (type, params) {
    if (type === AUTH_LOGIN) {
        return fetch('http://localhost:4000/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            // mode: 'no-cors',
            body: JSON.stringify({
                query: `mutation { login(token: "${params.token}", selectedShop: "${params.selectedShop}") { role displayName } }`
            }),
        }).then(res => res.json()).then(({ data: { login } }) => {
            sessionStorage.setItem('userToken', params.token);
            sessionStorage.setItem('userName', login.displayName);
            sessionStorage.setItem('userRole', login.role);
        });
    }
    if (type === AUTH_LOGOUT) {
        return signOut();
    }
    if (type === AUTH_ERROR) {
        console.error('AUTH_ERROR', params);
        return signOut().then(Promise.reject);
    }
    if (type === AUTH_CHECK || type === AUTH_GET_PERMISSIONS) {
        const role = sessionStorage.getItem('userRole');
        return role ? Promise.resolve(role) : Promise.reject();
    }
    return Promise.reject('Unknown method');
};

const styles = theme => ({
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'flex-start',
        background: 'url(https://source.unsplash.com/random/1600x900)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    card: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '6em',
    },
    progress: {
        
    }
});

class LoginComponent extends React.Component {
    constructor(props) {
        super(props);
        // firebaseApp.auth().onAuthStateChanged(
        //     (...args) => console.log(args)
        // );
        this.uiConfig = {
            // signInFlow: 'popup',
            signInOptions: [
                firebase.auth.EmailAuthProvider.PROVIDER_ID,
                firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                'anonymous'
            ],
            callbacks: {
                signInSuccessWithAuthResult: (authResult) => {
                    authResult.user.getIdToken().then(token => {
                        this.props.userLogin({
                            token,
                            // cSpell:disable
                            selectedShop: 'cjuc5l00m006407481l32ugz3'
                        });
                    });
                    return false;
                },
            },
        };
        this.state = {
            progress: false
        };
    }

    componentDidMount() {
        this.observer = new MutationObserver((mutationsList) => {
            this.setState({
                progress: !mutationsList[0].target.hasChildNodes()
            });
        });
        this.observer.observe(document.getElementById('firebaseui_container'), { childList: true });
    }

    componentWillUnmount() {
        this.observer.disconnect();
    }

    render() {
        const { classes } = this.props;
        const cardStyle = {
            backgroundColor: this.state.progress ? 'transparent' : '#fff'
        };
        return (
            <div className={classes.main}>
                <Card className={classes.card} style={cardStyle}>
                    <StyledFirebaseAuth
                        // className={styles.firebaseUi}
                        uiConfig={this.uiConfig}
                        // uiCallback={ui => { this.ui = ui }}
                        firebaseAuth={firebaseApp.auth()} />
                    {this.state.progress && <CircularProgress
                        color='secondary'
                        size={200}
                        thickness={5}
                        className={classes.progress} />}
                </Card>
                <Notification />
            </div>
        );
    }
};
// thehalcyonsavant@gmail.com
// test123
export const Login = withStyles(styles)(connect(null, { userLogin })(LoginComponent));

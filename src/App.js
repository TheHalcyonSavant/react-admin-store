import React, { Component } from 'react';
import { Admin, Resource, Loading } from 'react-admin';
import get from 'lodash/get';

import buildPrismaProvider, { buildQuery } from 'ra-data-opencrud';
import overriddenQueries from './queries/index';

import { ProductEdit, ProductList } from './components/products/index';
import { ShopEdit, ShopList } from './components/shops/index';
import { OrderList } from './components/orders/index';
import { CategoryCreate, CategoryEdit, CategoryList, CategoryShow } from './components/categories/index';
import { BrandCreate, BrandEdit, BrandList, BrandShow } from './components/brands/index';
import {
  AttributeCreate,
  AttributeEdit,
  AttributeList,
  AttributeShow
} from './components/attributes/index';
import { OptionCreate, OptionEdit, OptionList, OptionShow } from './components/options/index';

import './App.css';

import { authProvider, client, Login } from './firebase';

const enhanceBuildQuery = (buildQuery) => (introspectionResults) => (fetchType, resourceName, params) => {
  const fragment = get(overriddenQueries, `${resourceName}.${fetchType}`);

  return buildQuery(introspectionResults)(fetchType, resourceName, params, fragment);
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { dataProvider: null };
  }

  componentDidMount() {
    buildPrismaProvider({
      client,
      buildQuery: enhanceBuildQuery(buildQuery)
    }).then(dataProvider => this.setState({ dataProvider }));
  }

  render() {
    const { dataProvider } = this.state;

    if (!dataProvider) {
      return <Loading className="master-loading" loadingPrimary="Loading" loadingSecondary="The page is loading, just a moment please" />;
    }

    return (
      <Admin title="Prisma e-commerce" dataProvider={dataProvider} authProvider={authProvider} loginPage={Login}>
        {(permissions) => [
          <Resource name="Product" list={ProductList} edit={ProductEdit} />,
          <Resource name="Order" list={OrderList} />,
          <Resource
            name="Brand"
            list={BrandList}
            edit={BrandEdit}
            show={BrandShow}
            create={BrandCreate}
          />,
          <Resource
            name="Attribute"
            list={AttributeList}
            edit={AttributeEdit}
            show={AttributeShow}
            create={AttributeCreate}
          />,
          <Resource
            name="Category"
            list={CategoryList}
            edit={CategoryEdit}
            show={CategoryShow}
            create={CategoryCreate}
          />,
          <Resource name="Shop" {...(permissions === 'ADMIN' ? { list: ShopList, edit: ShopEdit } : {})} />,
          <Resource
            name="Option"
            list={OptionList}
            edit={OptionEdit}
            show={OptionShow}
            create={OptionCreate}
          />,
          <Resource name="OptionValue" />,
          <Resource name="SelectedOption" />,
          <Resource name="Variant" />,
          permissions === 'ADMIN' ? <Resource name="User" /> : null,
          <Resource name="OrderLineItem" />
        ]}
      </Admin>
    );
  }
}

export default App;
